<div id="welcome">
	<div class="row">
	 <div class="wlcLeft col-6 fl">
		 <h3>WELCOME TO</h3>
		 <h1>J-J Landscaping LLC</h1>
		 <h4>Your Dependable Lawncare & Landscaping Company</h4>
		 <p>J-J Landscaping, LLC has a proven track record of providing creative, high quality landscaping services. Our excellent landscape maintenance, gardening service, irrigation service, power washing, and lawn services have brought smiles to countless clients over the years. Homeowners who have had the pleasure of working with our professional staff find that we not only produce a better landscape environment, but our services often save them money in the long run. We provide the ultimate value for our clients’ investments in professional landscaping services.</p>
	 </div>
	 <div class="wlcRight col-6 fl">
		 <img src="public/images/content/wlcImg.jpg" alt="Lot">
	 </div>
	 <div class="clearfix"></div>
	</div>
</div>
<div id="about">
	<div class="row">
		<div class="abtLeft col-7 fl">
			<img src="public/images/content/aboutImg.jpg" alt="Driver">
		</div>
		<div class="abtRight col-5 fl">
			<h1>About <span>Us</span> </h1>
			<p>At J-J Landscaping, LLC, we constantly challenge ourselves to try something new, wonderful and interesting on each job we’re hired for. Based on the rave reviews we’ve received, we’re convinced that we are more than meeting the challenge. Our emphasis is on consistent quality, but we also appreciate the significant twists and turns that go along with creating a beautiful environment at each unique location.</p>
			<a href="about#content" class="btn">LEARN MORE</a>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div id="services">
	<div class="row">
		<div class="svcLeft col-5 fl">
			<h1>Our Services</h1>
			<ul>
					<li><span><a href="services#service1">Lawn Services</a></span></li>
					<li><span><a href="services#service2">Irrigation Service</a></span></li>
					<li><span><a href="services#service3">Gardening Service</a></span></li>
			</ul>
			<p>At J-J Landscaping, LLC, we constantly challenge ourselves to try something new, wonderful and interesting on each job we’re hired for.</p>
			<a href="services#content" class="button">Get A FREE QUOTE</a>
		</div>
		<div class="svcRight col-7 fr">
			<img src="public/images/content/svcImg1.jpg" alt="Services Images 1">
			<img src="public/images/content/svcImg2.jpg" alt="Services Images 2">
			<img src="public/images/content/svcImg3.jpg" alt="Services Images 3">
			<img src="public/images/content/svcImg4.jpg" alt="Services Images 4">
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div id="testimonials">
	<div class="row">
		<div class="testiLeft col-8 fl">
			<img src="public/images/content/testiImg1.jpg" alt="testimonials Image 1">
			<img src="public/images/content/testiImg2.jpg" alt="testimonials Image 2">
		</div>
		<div class="testiRight col-4 fr">
			<h1>Testimonials</h1>
			<dl>
				<dt> <p>Great company...<br> Friendly staff & affordable </p> </dt>
				<dd>
					<p> <span>&#x2605; &#x2605; &#x2605; &#x2605; &#x2605;</span> - Chauntelle D.</p>
				</dd>
			</dl>
			<dl>
				<dt> <p>Great company to trust . friendly with quality services .<br> Affordable and Very recommended  </p> </dt>
				<dd>
					<p> <span>&#x2605; &#x2605; &#x2605; &#x2605; &#x2605;</span> - Jessica B.</p>
				</dd>
			</dl>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div id="contact">
	<div class="row">
		<div class="cntLeft col-8 fl">
			<div class="text">
				<h1>Recent <span>Works</span> </h1>
				<img src="public/images/content/worksImg1.jpg" alt="Works Image 1"/>
				<img src="public/images/content/worksImg2.jpg" alt="Works Image 2"/>
				<img src="public/images/content/worksImg3.jpg" alt="Works Image 3"/>
				<img src="public/images/content/worksImg4.jpg" alt="Works Image 4"/>
			</div>
		</div>
		<div class="cntRight col-4 fr">
			<div class="cntPanel">
				<h1>Contact <span>Us</span> </h1>
				<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
					<input type="text" name="name" placeholder="Name:">
					<input type="text" name="address" placeholder="Address:">
					<input type="text" name="email" placeholder="Email:">
					<input type="text" name="phone" placeholder="Phone:">
					<textarea name="message" cols="30" rows="10" placeholder="Message:"></textarea>
					<p><input type="checkbox" name="consent" class="consentBox">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.</p>
					<?php if( $this->siteInfo['policy_link'] ): ?>
					<p><input type="checkbox" name="termsConditions" class="termsBox"/> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a></p>
					<div class="g-recaptcha"></div>
					<?php endif ?>
					<button type="submit" class="ctcBtn btn" disabled>SUBMIT FORM</button>
				</form>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
